import { dataShoe } from "../../DataShoe";
import {
    ADD_TO_CART,
    CHANGE_QUANTITY,
    SEE_DETAIL,
} from "../constants/ShoeConstant";

let initialState = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
};

export const shoeReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEE_DETAIL: {
            return { ...state, detail: action.payload };
        }
        case ADD_TO_CART: {
            let newCart = [...state.cart];

            let index = newCart.findIndex((item) => {
                return item.id === action.payload.id;
            });
            if (index === -1) {
                let newItem = { ...action.payload, number: 1 };
                newCart.push(newItem);
            } else {
                newCart[index].number++;
            }
            return { ...state, cart: newCart };
        }
        case CHANGE_QUANTITY: {
            let newCart = [...state.cart];
            let index = newCart.findIndex((item) => {
                return item.id === action.payload.id;
            });
            if (index !== -1) {
                newCart[index].number += action.payload.num;
                if (newCart[index].number === 0) {
                    newCart.splice(index, 1);
                }
            }

            return { ...state, cart: newCart };
        }
        default:
            return state;
    }
};
