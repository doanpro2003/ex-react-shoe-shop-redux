import React, { Component } from "react";
import Cart from "./Cart";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class ShoeShopRedux extends Component {
    render() {
        return (
            <div className="row container mx-auto">
                <Cart />
                <ListShoe />
                <DetailShoe />
            </div>
        );
    }
}
