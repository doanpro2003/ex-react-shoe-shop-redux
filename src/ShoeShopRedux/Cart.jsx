import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY } from "./redux/constants/ShoeConstant";

class Cart extends Component {
    renderTbody = () => {
        return this.props.cart.map((item, index) => {
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <img style={{ width: 50 }} src={item.image} alt="" />
                    </td>
                    <td>${item.price * item.number}</td>
                    <td>
                        <button
                            className="btn btn-danger "
                            onClick={() => {
                                this.props.handleTangGiamSoLuong(item.id, -1);
                            }}
                        >
                            -
                        </button>
                        <span className="mx-2">{item.number}</span>
                        <button
                            className="btn btn-success"
                            onClick={() => {
                                this.props.handleTangGiamSoLuong(item.id, 1);
                            }}
                        >
                            +
                        </button>
                    </td>
                </tr>
            );
        });
    };
    render() {
        return (
            <table className="table">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Image</td>
                        <td>Price</td>
                        <td>Quantity</td>
                    </tr>
                </thead>
                <tbody>{this.renderTbody()}</tbody>
            </table>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        cart: state.shoeReducer.cart,
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        handleTangGiamSoLuong: (id, num) => {
            let action = {
                type: CHANGE_QUANTITY,
                payload: {
                    id,
                    num,
                },
            };
            dispatch(action);
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
