import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, SEE_DETAIL } from "./redux/constants/ShoeConstant";

class ItemShoe extends Component {
    render() {
        let { image, name, price } = this.props.data;
        return (
            <div className="col-3 p-1">
                <div className="card text-left h-100">
                    <img className="card-img-top" src={image} alt="" />
                    <div className="card-body">
                        <h4 className="card-title">{name}</h4>
                        <h5 className="card-text">${price}</h5>
                        <button
                            className="btn btn-dark m-1"
                            onClick={() => {
                                this.props.handleAddToCart(this.props.data);
                            }}
                        >
                            Add Cart <i className="fa fa-shopping-cart"></i>
                        </button>
                        <button
                            onClick={() => {
                                this.props.handleSeeDetail(this.props.data);
                            }}
                            className="btn btn-dark "
                        >
                            See Detail
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleSeeDetail: (shoe) => {
            let action = {
                type: SEE_DETAIL,
                payload: shoe,
            };
            dispatch(action);
        },
        handleAddToCart: (shoe) => {
            let action = {
                type: ADD_TO_CART,
                payload: shoe,
            };
            dispatch(action);
        },
    };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
